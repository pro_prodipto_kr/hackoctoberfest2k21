public class Example{  
   public static void main(String args[]){  
	//empty string
	String str1="";  
	//non-empty string
	String str2="hello";  

	//prints true
	System.out.println(str1.isEmpty());  
	//prints false
	System.out.println(str2.isEmpty());  
   }
}